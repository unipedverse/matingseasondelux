﻿using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
public class ContentController : MonoBehaviour
{
    /// <summary>
    /// Contents to be controlled
    /// </summary>
    private Transform[] contents;
    /// <summary>
    /// wether the content has been disabled by this script, or by another source
    /// </summary>
    private Dictionary<Transform, bool> externalDisable = new Dictionary<Transform, bool>();
    private void Start()
    {
        //children of the object this is script is attatched to are the contents
        contents = this.GetComponentsInChildren<Transform>(true).Where(x => x.parent == this.transform).ToArray();
        //fill the dictionary
        contents.ForEach(x => externalDisable.Add(x, true));
    }

    /// <summary>
    /// Switch the current content
    /// </summary>
    /// <param name="index">
    /// index of the content
    /// </param>
    public void SwitchContent(int index)
    {
        //loop through contents
        for (int i = 0; i < contents.Length; i++)
        {
            //check if wanted content found
            if (i == index)
            {
                //activate it
                contents[i].gameObject.SetActive(true);
                externalDisable[contents[i]] = false;
            }
            else contents[i].gameObject.SetActive(false); //if not wanted content, disable it
        }
    }
    /// <summary>
    /// Toggles the given content on or off
    /// </summary>
    /// <param name="index">
    /// index of the content
    /// </param>
    public void ToggleContent(int index)
    {
        //loop through contents
        for (int i = 0; i < contents.Length; i++)
        {
            //check if wanted content found
            if (i == index)
            {
                //check if content is active
                if (contents[i].gameObject.activeSelf)
                {
                    //if so, disable it
                    contents[i].gameObject.SetActive(false);
                    //content disabled by an external source
                    externalDisable[contents[i]] = true;
                }
                else
                {
                    //if not active
                    //check if it's been disabled from the outside
                    if (externalDisable[contents[i]])
                    {
                        //if so, activate it
                        contents[i].gameObject.SetActive(true);
                        externalDisable[contents[i]] = false;
                    }
                    //otherwise, say it's been disabled from the outside
                    else externalDisable[contents[i]] = true;
                }
            }
            else //if not wanted content, disable it
                contents[i].gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// Switch the current content
    /// </summary>
    /// <param name="contentNameContains">
    /// string contained in the content's name
    /// </param>
    public void SwitchContent(string contentNameContains)
    {
        //loop through contents
        for (int i = 0; i < contents.Length; i++)
        {
            //check if wanted content found
            if (contents[i].name.Contains(contentNameContains))
            {
                //activate it
                contents[i].gameObject.SetActive(true);
                externalDisable[contents[i]] = false;
            }
            else contents[i].gameObject.SetActive(false);//if not wanted content, disable it
        }
    }
    /// <summary>
    /// Toggles the given content on or off
    /// </summary>
    /// <param name="contentNameContains">
    /// string contained in the content's name
    /// </param>
    public void ToggleContent(string contentNameContains)
    {
        //loop through contents
        for (int i = 0; i < contents.Length; i++)
        {
            //check if wanted content found
            if (contents[i].name.Contains(contentNameContains))
            {
                //check if content is active
                if (contents[i].gameObject.activeSelf)
                {
                    //if so, disable it
                    contents[i].gameObject.SetActive(false);
                    //content disabled by an external source
                    externalDisable[contents[i]] = true;
                }
                else
                {
                    //if not active
                    //check if it's been disabled from the outside
                    if (externalDisable[contents[i]])
                    {
                        //if so, activate it
                        contents[i].gameObject.SetActive(true);
                        externalDisable[contents[i]] = false;
                    }
                    //otherwise, say it's been disabled from the outside
                    else externalDisable[contents[i]] = true;
                }
            }
            else //if not wanted content, disable it
                contents[i].gameObject.SetActive(false);
        }
    }


}
