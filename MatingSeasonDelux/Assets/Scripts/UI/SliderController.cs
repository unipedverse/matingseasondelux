﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Extensions;

public class SliderController : INeedyAwake
{
    public bool useInt;
    public bool snap;
    public float snapNearValue;
    public float snapStrength;
    [HideInInspector]
    public float value;

    private Slider slider;
    private TextMeshProUGUI text;
    private bool canChange = true;
    private bool recentlyIncremented;
    public event Action<SliderController> OnValueChange = delegate
    {
    };
    public override void WakeMeUpInside()
    {
        slider = this.gameObject.GetComponentInChildren<Slider>();
        text = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OverWriteValue(float newVal)
    {
        slider.value = newVal;
    }

    public void IncrementValue(float increment)
    {
        slider.value += increment;
        if (snap)
        {
            recentlyIncremented = true;
            snap = false;
        }
    }
    public void OnValueChanged()
    {
        if (canChange)
        {
            if (snap)
            {
                if (slider.value.InRange(snapNearValue, snapStrength))
                    slider.value = snapNearValue;
            }
            else
            {
                if (recentlyIncremented)
                {
                    snap = true;
                    recentlyIncremented = false;
                }
            }
            value = slider.value;
            OnValueChange(this);
        }
    }
}
