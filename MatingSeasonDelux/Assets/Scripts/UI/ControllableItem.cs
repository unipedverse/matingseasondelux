﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
public class ControllableItem : INeedyAwake
{
    private RectTransform _rect;
    public RectTransform Rect { get { return _rect; } }
    public ControlOption[] options;


    public override void WakeMeUpInside()
    {
        _rect = this.GetComponent<RectTransform>();
        options.ForEach(x =>
        {
            x.controller.OnValueChange -= OnValueChange;
            x.controller.OnValueChange += OnValueChange;
        });
    }

    private void OnValueChange(SliderController controller)
    {
        ControlOption option = options.First(x => x.controller == controller);
        int multiplier = option.useNegative ? -1 : 1;
        switch (option.value)
        {
            case ValueControl.SizeX:
                this.Rect.localScale = new Vector2(option.startValue + option.controller.value * multiplier, Rect.localScale.y);
                break;
            case ValueControl.SizeY:
                this.Rect.localScale = new Vector2(Rect.localScale.x, option.startValue + option.controller.value * multiplier);
                break;
            case ValueControl.PositionX:
                this.Rect.localPosition = new Vector2(option.startValue + option.controller.value * multiplier, Rect.localPosition.y);
                break;
            case ValueControl.PositionY:
                this.Rect.localPosition = new Vector2(Rect.localPosition.x, option.startValue + option.controller.value * multiplier);
                break;
            case ValueControl.Rotation:
                this.Rect.localEulerAngles = new Vector3(Rect.localEulerAngles.x, Rect.localEulerAngles.y, option.startValue + option.controller.value * multiplier);
                break;
        }
    }
}
