﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

/// <summary>
/// Container for Slider Links
/// </summary>
public class SliderLinkController : INeedyAwake
{
    /// <summary>
    /// Links that will be set in the scene
    /// </summary>
    public SliderLink[] links;

    public override void WakeMeUpInside()
    {
        //Call the NeedyAwake function of all the links
        links.ForEach(x => x.WakeMeUpInside());
    }
    /// <summary>
    /// Switch the state of the links to be on (if off) or off (if on)
    /// </summary>
    public void ToggleLinks()
    {
        links.ForEach(x => x.OnToggle());
    }
}

/// <summary>
/// Class with which to Link SliderControllers
/// </summary>
[Serializable]
public class SliderLink 
{
    /// <summary>
    /// Slider Controllers
    /// </summary>
    public SliderController[] controllers;
    /// <summary>
    /// Starting state of the link
    /// </summary>
    public bool initialState;
    /// <summary>
    /// Is the link active?
    /// </summary>
    private bool isOn;

    public void WakeMeUpInside()
    {
        //Set the current state to the given intial state
        isOn = initialState;
        if (isOn)
            controllers.ForEach(x => x.OnValueChange += ValueChanged); //Bind the ValueChanged function to that of the Sliders
        else controllers.ForEach(x => x.OnValueChange -= ValueChanged); //Unbind the function to make sure it doesn't get called twiche

    }
    /// <summary>
    /// Toggle the link on and off
    /// </summary>
    public void OnToggle()
    {
        isOn = !isOn;
        if (isOn)
        {
            controllers.ForEach(x =>
            {
                //Force the controller to take the value of the first controller in the array
                x.OverWriteValue(controllers[0].value);
                //Bind the ValueChanged function to the OnValueChange event
                x.OnValueChange += ValueChanged;
            });
        }
        else //if it's been deactivated, unbind the listener
            controllers.ForEach(x => x.OnValueChange -= ValueChanged);
    }

    /// <summary>
    /// Listener for the OnValueChange of the SliderController
    /// </summary>
    /// <param name="controller">
    /// The controller that had it's value changed
    /// </param>
    private void ValueChanged(SliderController controller)
    {
        controllers.ForEach(x =>
        {
            if (x.value != controller.value) //check to see if current slider already has the value of the controller that called the function
                x.OverWriteValue(controller.value); //if not, overwrite it
        });
    }
}


