﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Extensions;

public class Dropdown : INeedyAwake
{
    public bool isActive;
    public Transform[] ignore;
    private RectTransform rect;
    private ContentSizeFitter fitter;
    private Transform[] contents;
    private Dropdown[] childrenDropdowns;
    public float MinimumY
    {
        get
        {
            return Math.Abs(rect.GetComponentsInChildren<RectTransform>(true).Min(x => x.localPosition.y));
        }
    }
    public override void WakeMeUpInside()
    {
        contents = this.GetComponentsInChildren<Transform>(true).Where(x => x != this.transform && x.parent == this.transform && !ignore.Contains(x)).ToArray();
        fitter = this.GetComponentInParent<ContentSizeFitter>();
        childrenDropdowns = contents.Where(x => x.GetComponent<Dropdown>() != null).Select(x => x.GetComponent<Dropdown>()).ForEach(x => { x.fitter = this.fitter; return x; }).ToArray();
        rect = this.GetComponent<RectTransform>();
    }
    
    public void OnButton()
    {
        isActive = !isActive;
        int childIndex = this.transform.GetSiblingIndex();
        if (!isActive)
        {
            childrenDropdowns.Where(x => x.isActive).ForEach(x => x.OnButton());
        }
        contents.ForEach(x =>
        {
            x.SetParent(isActive ? fitter.transform : this.transform);
            childIndex++;
            x.SetSiblingIndex(childIndex);
            x.gameObject.SetActive(isActive);
        });

    }

    

}

