﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public enum ValueControl
{
    SizeX,
    SizeY,
    PositionX,
    PositionY,
    Rotation
}
[Serializable]
public class ControlOption
{
    public ValueControl value;
    public SliderController controller;
    public float startValue;
    public bool useNegative;
}