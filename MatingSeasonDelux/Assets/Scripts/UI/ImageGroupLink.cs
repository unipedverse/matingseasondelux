﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class ImageGroupLink : INeedyAwake
{

    public ImageGroupController[] groups;
    public bool initialState;
    private bool isOn;

    public override void WakeMeUpInside()
    {
        isOn = initialState;
        if (isOn)
            groups.ForEach(x => x.OnValueChanged += ValueChanged);
        else groups.ForEach(x => x.OnValueChanged -= ValueChanged);

    }

    public void OnToggle(bool toggle)
    {
        isOn = toggle;
        if (isOn)
        {
            groups.ForEach(x =>
            {
                x.SwitchToOption(groups[0].optionIndex);
                x.OnValueChanged += ValueChanged;
            });
        }
        else groups.ForEach(x => x.OnValueChanged -= ValueChanged);
    }

    private void ValueChanged(ImageGroupController controller)
    {
        groups.ForEach(x =>
        {
            if (x.currentOption != controller.currentOption)
                x.SwitchToOption(controller.optionIndex);
        });
    }
}
