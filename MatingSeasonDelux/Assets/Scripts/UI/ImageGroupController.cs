﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;
public class ImageGroupController : INeedyAwake
{
    private TextMeshProUGUI text;
    [HideInInspector]
    public int optionIndex;
    public Sprite[] options;
    public Sprite currentOption;
    public Image[] targetGraphics;
    public event Action<ImageGroupController> OnValueChanged = delegate
    {
    };
    
    public override void WakeMeUpInside()
    {
        text = this.GetComponentsInChildren<TextMeshProUGUI>().First(x => x.rectTransform.parent.name == "TextPivot");
            if (options != null && options.Count() > 0)
                currentOption = options[0];
        text.text = optionIndex.ToString();
        List<Sprite> sprites = options.ToList();
        List<Image> images = targetGraphics.ToList();
        sprites.TrimExcess();
        images.TrimExcess();
        options = sprites.ToArray();
        targetGraphics = images.ToArray();
    }
    public void ChangeOption(int change)
    {
        optionIndex += change;
        if (optionIndex < 0)
            optionIndex = options.Length - 1;
        else if (optionIndex > options.Length - 1)
            optionIndex = 0;
        text.text = optionIndex.ToString();
        currentOption = options[optionIndex];
        targetGraphics.ForEach(x => x.sprite = currentOption);
        OnValueChanged(this);
    }
    public void SwitchToOption(int optionIndex)
    {
        this.optionIndex = optionIndex;
        currentOption = options[this.optionIndex];
        text.text = this.optionIndex.ToString();
        targetGraphics.ForEach(x => x.sprite = currentOption);
    }

}
