﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Class to derive from in case an object needs to set variables even if disabled
/// </summary>
public abstract class INeedyAwake : MonoBehaviour
{
   public abstract void WakeMeUpInside();
}
