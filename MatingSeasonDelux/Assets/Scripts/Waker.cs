﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
public class Waker : MonoBehaviour
{
    private void Awake()
    {
        INeedyAwake[] needyAwakes = this.GetComponentsInChildren<INeedyAwake>(true);
        needyAwakes.ForEach(x => x.WakeMeUpInside());
    }
}

